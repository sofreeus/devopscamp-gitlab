#!/usr/bin/env bash

sudo apt-add-repository ppa:ansible/ansible
sudo apt update && sudo apt install ansible

ansible-galaxy install geerlingguy.docker geerlingguy.pip

sudo ansible-playbook -i ./hosts ./docker.yaml
