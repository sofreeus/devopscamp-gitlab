#!/usr/bin/env bash

sudo apt-get install apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get update

sudo apt-get install docker-ce

for USR in $(sudo getent group wheel | awk -F: '{print $4}' | tr , \ )
  do sudo usermod -aG docker $USR
done

docker-compose up -d
